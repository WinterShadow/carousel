'use strict';

var Backbone = require('backbone');
var objectAssign = require('object-assign');
var randomFromArray = require('../lib/random-from-array');

module.exports = Backbone.Model.extend({
  defaults: {
    title: '',
    images: [],
    image: ''
  },
  parse: function (rawData) {
    return objectAssign(rawData, { image: randomFromArray(rawData.images) });
  }
});
