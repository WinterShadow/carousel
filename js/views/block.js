'use strict';

var _ = require('underscore');
var $ = require('jquery');
var Backbone = require('backbone');

var tmpl = require('../templates/block.html');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  tagName: 'div',
  attributes: {
    class: 'image'
  },
  template: _.template(tmpl),
  render: function () {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  }
});
