'use strict';

var _ = require('underscore');
var $ = require('jquery');
var Backbone = require('backbone');

var BlockCollection = require('../collections/blocks.js');
var BlockView = require('./block.js');

var DEFAULTS = {
  groupIndex: 0,
  serverUrl: 'http://localhost:8080/blocks',
  blocksPerGroup: 4
};

module.exports = Backbone.View.extend({
  el: '.carousel-container',
  events: {
    'click .prev': 'showPrev',
    'click .next': 'showNext'
  },
  initialize: function (options) {
    this.options = _.defaults(options || {}, DEFAULTS);
    this.groupIndex = this.options.groupIndex;
    this.$prev = this.$el.find('.prev');
    this.$next = this.$el.find('.next');
    this.$imagesContainer = this.$el.find('.images');
    this.blocks = new BlockCollection(null, {
      url: this.options.serverUrl
    });

    this.listenTo(this.blocks, 'reset', this.render);

    this.blocks.fetch({ reset: true });
  },
  render: function () {
    var from = this.groupIndex * this.options.blocksPerGroup;
    var to = from + this.options.blocksPerGroup;
    var modelsToRender = this.blocks.slice(from, to);

    var elements = _.map(modelsToRender, function (model) {
      return new BlockView({ model: model }).render().el;
    });

    this.$imagesContainer.html(elements);

    this.setButtonState(this.$prev, this.groupIndex);
    this.setButtonState(this.$next, to < this.blocks.models.length);
  },
  setButtonState: function ($btn, isActive) {
    if (isActive) {
      $btn.removeAttr('disabled');
    } else {
      $btn.attr('disabled', 'disabled');
    }
  },
  showPrev: function (e) {
    var newIndex = this.groupIndex && this.groupIndex - 1;
    this.show(e, newIndex);
  },
  showNext: function (e) {
    this.show(e, this.groupIndex + 1);
  },
  show: function (e, groupIndex) {
    e.preventDefault();
    this.groupIndex = groupIndex;
    this.render();
  }
});
