'use strict';

var Backbone = require('backbone');
var BlockModel = require('../models/block.js');

module.exports = Backbone.Collection.extend({
  model: BlockModel,
  initialize: function (models, options) {
    this.url = options.url;
  }
});
