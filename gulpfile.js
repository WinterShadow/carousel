'use strict';

var gulp = require('gulp');
var browserify = require('browserify');
var stringify = require('stringify');
var source = require('vinyl-source-stream');
var less = require('gulp-less');
var path = require('path');
var src = './js/app.js';
var lessSrc = './styles/**/*.less';
var targetDir = 'public/js/';


gulp.task('default', function () {
  return browserify(src)
    .transform(stringify, {
      appliesTo: { includeExtensions: ['.html'] } 
    })
    .bundle()
    .on('error', function (err) {
      console.log(err.message);
      this.emit('end');
    })
    .pipe(source('bundle.js'))
    .pipe(gulp.dest(targetDir));
});

gulp.task('less', function () {
  return gulp.src(lessSrc)
    .pipe(less({
      paths: [path.join(__dirname, 'less', 'includes')]
    }))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('watch', function () {
  gulp.watch('./js/**/*.*', ['default']);
  gulp.watch(lessSrc, ['less']);
});
